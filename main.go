package main

import (
	"delivery_service/handler"
	"fmt"
	"github.com/micro/micro/v3/service"
	"github.com/micro/micro/v3/service/logger"

	pb "gitlab.com/devops-experimental/test-grpc/proto"
)

func main() {
	// Create service
	srv := service.New(
		service.Name("delivery-service"),
	)

	// Register Handler
	pb.RegisterDeliveryServiceHandler(srv.Server(), &handler.DeliveryHandler{})

	fmt.Println("Order service is running...")

	// Run the service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
