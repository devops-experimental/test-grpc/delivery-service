build:
	docker build --tag ${DOCKER_IMG_TAG} --no-cache .

up:
	docker compose up -d

down:
	docker compose down

push:
	docker push $(DOCKER_IMG_TAG)

logs:
	docker compose logs -f
