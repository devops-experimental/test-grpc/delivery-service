package handler

import (
	"context"
	"fmt"
	pb "gitlab.com/devops-experimental/test-grpc/proto"
)

type DeliveryHandler struct{}

func (s *DeliveryHandler) DeliverOrder(ctx context.Context, request *pb.DeliverOrderRequest, response *pb.DeliveryResponse) error {
	orderID := request.GetOrderId()
	message := fmt.Sprintf("Order %s has been delivered.", orderID)
	response.Message = message
	return nil
}
